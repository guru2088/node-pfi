'use strict';
const mongoose = require('mongoose');
var util = require('util');
var userDetails = require('../db/model/userDetails');
var userDetailsSchema = mongoose.model('userDetails');
var dbcon = require("../db/dbcon");

/* GET */
function login(req, res){

  var condition = {
    loginDetails:{
      uName: req.swagger.params.uName.value,
      saltPassword: req.swagger.params.saltPassword.value
    }
  };

  dbcon.getData(userDetailsSchema,condition, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}

/* POST */
function register(req, res){

  var email = req.swagger.params.email.value;
  var email_formatted = util.format('%s!', email);

  const userDetailsObj_register = new userDetailsSchema({
    userID: 1,
    name: {
      fName:req.swagger.params.fName.value,
      lName:req.swagger.params.lName.value
    },
    email:email_formatted,
    mobile:req.swagger.params.mobile.value,
    createdDate: new Date()
  });

  dbcon.saveData(userDetailsObj_register,"userDetails", function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}

/* POST */
function profile(req, res){

  var conditions = { userID: req.swagger.params.userID.value };

  var updatequery = {
    loginDetails: {
      uName:req.swagger.params.uName.value,
      saltPassword:req.swagger.params.saltPassword.value
    },
    firmName:req.swagger.params.firmName.value,
    dateofBirth:req.swagger.params.dateofBirth.value,
    homeaddr1:req.swagger.params.homeaddr1.value,
    homeaddr2:req.swagger.params.homeaddr2.value,
    pincode:req.swagger.params.pincode.value,
    state:req.swagger.params.state.value,
    gstNo:req.swagger.params.gstNo.value,
    bankDetails: {
      ifsc:req.swagger.params.ifsc.value,
      accNo:req.swagger.params.accNo.value
    },
    updateDate: new Date()
  };

  dbcon.updateData(userDetailsSchema,conditions,updatequery, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}

/* POST */
function AdminApprove(req, res){

  var conditions = { userID: req.swagger.params.userID.value };

  var updatequery = {
    AdminApprove: 1
  };

  dbcon.updateData(userDetailsSchema,conditions,updatequery, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}


module.exports = {
  login: login,
  register: register,
  profile: profile,
  AdminApprove:AdminApprove
};
