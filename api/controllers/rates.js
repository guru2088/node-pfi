'use strict';
const mongoose = require('mongoose');
var util = require('util');
var materialDetails = require('../db/model/materialDetails');
var materialDetailsSchema = mongoose.model('materialDetails');
var dbcon = require("../db/dbcon");

function getrates(req, res){

  var condition = {};

  dbcon.getData(materialDetailsSchema,condition, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}


function saverate(req, res){

  var materialObj = {
    "08mm":req.swagger.params.eight.value,
    "10mm":req.swagger.params.ten.value,
    "12mm":req.swagger.params.twelve.value,
    "16mm":req.swagger.params.sixteen.value,
    "20mm":req.swagger.params.twenty.value,
    "25mm":req.swagger.params.twentyfive.value,
    "28mm":req.swagger.params.twentyeight.value,
    state:req.swagger.params.state.value
  };

  var condition = { state:req.swagger.params.state.value }

  dbcon.updateData(materialDetailsSchema,condition,materialObj, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}

/* GET */

module.exports = {
  getrates: getrates,
  saverate: saverate
};
