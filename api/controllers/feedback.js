'use strict';
const mongoose = require('mongoose');
var util = require('util');
var feedback = require('../db/model/feedback');
var feedbackSchema = mongoose.model('feedback');
var dbcon = require("../db/dbcon");

/* POST */
function saveFeedback(req, res){

  const feedbackObj = new feedbackSchema({
    feedbackID: 1,
    name:req.swagger.params.name.value,
    email:req.swagger.params.email.value,
    mobile:req.swagger.params.mobile.value,
    comments:req.swagger.params.comments.value,
    createdDate: new Date()
  });

  dbcon.saveData(feedbackObj,"feedback", function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}

function getFeedbacks(req, res){
  var condition = {};

  dbcon.getData(feedbackSchema,condition, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });
}


module.exports = {
  saveFeedback: saveFeedback,
  getFeedbacks : getFeedbacks
};
