'use strict';
const mongoose = require('mongoose');
var util = require('util');
var materialDetails = require('../db/model/materialDetails');
var materialDetailsSchema = mongoose.model('materialDetails');
var orderDetails = require('../db/model/orderDetails');
var orderDetailsSchema = mongoose.model('orderDetails');
var dbcon = require("../db/dbcon");


/* POST */
function saveorder(req, res){

  const orderObj = new orderDetailsSchema({
    orderID: 2,
    orderDate: new Date(),
    orderDIA: req.swagger.params.orderDIA.value,
    orderWeight: req.swagger.params.orderWeight.value,
    orderPrice: req.swagger.params.orderPrice.value,
    userID:req.swagger.params.userID.value,
    totalPrice:req.swagger.params.totalPrice.value,
    orderStatus:"Ordered"
  });

  dbcon.saveData(orderObj,"orderDetails", function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}

/* POST */
function updateUTR(req, res){

  var conditions = { orderID: req.swagger.params.orderID.value };

  var updatequery = {
    paymentDetails: {
      utrNo:req.swagger.params.utrNo.value
    },
    updateDate: new Date()
  };

  dbcon.updateData(orderDetailsSchema,conditions,updatequery, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}

/* GET */
function getOrder_orderID(req, res){

  var condition = {orderID: req.swagger.params.orderID.value};
  dbcon.getData(orderDetailsSchema,condition, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });
}

/* GET */
function getOrder_userID(req, res){
  var condition = {userID: req.swagger.params.userID.value};
  dbcon.getData(orderDetailsSchema,condition, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}

module.exports = {
  saveorder: saveorder,
  updateUTR: updateUTR,
  getOrder_orderID:getOrder_orderID,
  getOrder_userID:getOrder_userID
};
