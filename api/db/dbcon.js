const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/PFI');
var db = mongoose.connection;

db.on('error', function (err) {
console.log('connection error', err);
});

db.once('open', function () {
console.log('connected.');
});

function saveData(obj,coll,callback) {
    obj.save(function (err, coll) {
       if (!err){
         return callback("Record saved successfully")
       }
       else{
         return callback(err)
       }
    });
}

function updateData(obj,conditions,updatequery,callback) {
    obj.updateOne(conditions,updatequery,{upsert:true}, function(err, res) {
      if (!err){
        return callback("Record updated successfully")
      }
      else{
        return callback(err)
      }
    });
}

function getData(obj,condition,callback) {
    obj.find(condition, function(err, res) {
      if (!err){
        return callback(res)
      }
      else{
        return callback(err)
      }
    });
}

function dropCollection(obj,callback) {
    obj.collection.drop(function(err, res) {
      if (!err){
        return callback(res)
      }
      else{
        return callback(err)
      }
    });
}

function findOneAndUpdate(obj,condition,doc,callback) {

    obj.findOneAndUpdate(condition, doc, { upsert: true }, function(err, res) {
      if (!err){
        return callback(res)
      }
      else{
        return callback(err)
      }
    });
}


function cleanup() {
  mongoose.disconnect();
  return true;
}

module.exports = {
  saveData: saveData,
  updateData: updateData,
  getData:getData,
  dropCollection:dropCollection,
  findOneAndUpdate:findOneAndUpdate
};
