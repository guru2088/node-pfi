const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const officeAddress = new Schema({
  id:Number,
  address1:String,
  address2:String,
  address3:String,
  pincode:Number,
  stateID:Number
});

mongoose.model('officeAddress', officeAddress);
