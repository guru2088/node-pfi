const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userDetails = new Schema({
  userID:{
    type:Number
  },
  name:{
   fName:{
     type:String
   },
   lName:{
     type:String
   }
  },
  email:{
    type:String,
    unique:true
  },
  saltPassword:{
    type:String
  },
  mobile:{
    type:Number,
    unique:true
  },
  firmName:{
    type:String
  },
  dateofBirth:{
    type:Date,
  },
  address1:{
    type:String,
  },
  address2:{
    type:String,
  },
  address3:{
    type:String,
  },
  pincode:{
    type:String,
  },
  stateID:{
    type:String,
  },
  gstNo:{
    type:String,
    unique:true
  },
  userType:{
    type:String,
  },
  createdAt:{
    type:Date,
    default: Date.now
  },
  modifiedAt:{
    type:Date
  }
});

module.exports = mongoose.model('userDetails', userDetails);
