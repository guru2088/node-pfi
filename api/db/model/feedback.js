const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const feedback = new Schema({
  id:Number,
  name:String,
  email:String,
  mobile:Number,
  comments:String,
  createdAt:{
    type:Date,
    default: Date.now
  }
});

mongoose.model('feedback', feedback);
