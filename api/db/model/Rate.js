const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const materialDetails = new Schema({
  "id":Number,
  "value":String,
  "rate":Number,
  "type":String,
  "distributorID":Number,
  createdAt:{
    type:Date,
    default: Date.now
  },
  modifiedAt:{
    type:Date
  }
});

mongoose.model('materialDetails', materialDetails);
