const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderDetails = new Schema({
  orderID:{
    type:String,
    required: true,
    unique:true
  },
  orderDate: Date,
  OrderProductAttributes: {
    value : String,
    weight: Number,
    price:Number
  },
  userID:Number,
  totalPrice:Number,
  status:String,
  paymentDetails:{
    amount:String
    utrNo:String
  },
  cgst:Number,
  sgst:Number,
  igst:Number,
  deliveryAddress:{
    address1:String,
    address2:String,
    address3:String,
    pincode:Number,
    stateID:Number
  }
  createdAt:{
    type:Date,
    default: Date.now
  },
  modifiedAt:{
    type:Date
  }
});

mongoose.model('orderDetails', orderDetails);
